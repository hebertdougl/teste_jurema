$(window).scroll(function() {
  var scrolled = $(window).scrollTop()
  $('.parallax').each(function(index, element) {
    var initY = $(this).offset().top;
    var height = $(this).height();
    var endY  = initY + $(this).height();

    var diff = scrolled - initY;
    var ratio = Math.round((diff / height) * 100);
    $(this).css('background-position','center ' + parseInt(-(ratio * 1.5)) + 'px');
  })
})