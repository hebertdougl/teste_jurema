class Shark < Fish
  validates_presence_of :color

  def self.findByColor(color)
    raise "wrong type: string required" unless color.is_a?(String)
    Shark.where(color: color)
  end
end
