class Tuna < Fish
  validates_presence_of :origin

  def self.findByOrigin(origin)
    raise "wrong type: string required" unless origin.is_a?(String)
    Tuna.where(origin: origin)
  end
end
