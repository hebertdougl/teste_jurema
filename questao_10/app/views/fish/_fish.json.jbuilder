json.extract! fish, :id, :name, :origin, :color, :created_at, :updated_at
json.url fish_url(fish, format: :json)
