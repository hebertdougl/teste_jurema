require 'rails_helper'

RSpec.describe Shark, type: :model do
  subject {
    described_class.new(
      name: "Shark name",
      origin: "Shark origin",
      color: "white"
    )
  }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without a color" do
      subject.color = nil
      expect(subject).to_not be_valid
    end
  end

  describe "findByColor" do
    it "is not valid wrong params type" do
      expect{ Shark.findByColor(123) }.to raise_error("wrong type: string required")
    end

    it "not exists data" do
      expect(Shark.findByColor("black")).to eq []
    end

    it "exists" do
      Shark.create!(
        name: "Shark name",
        origin: "Shark origin",
        color: "white"
      )
      expect(Shark.findByColor("white")).to eq [Shark.last]
    end
  end

end
