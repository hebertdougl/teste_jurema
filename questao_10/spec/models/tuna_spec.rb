require 'rails_helper'

RSpec.describe Tuna, type: :model do
  subject {
    described_class.new(
      name: "Tuna Name",
      origin: "Tuna Origin",
      color: "Tuna color"
    )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a name" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a origin" do
    subject.origin = nil
    expect(subject).to_not be_valid
  end

  it "is valid without color" do
    subject.color = nil
    expect(subject).to be_valid
  end

  describe "findByOrigin" do
    it "is not valid wrong params type" do
      expect{ Tuna.findByOrigin(123) }.to raise_error("wrong type: string required")
    end

    it "not exists data" do
      expect(Tuna.findByOrigin("Origin A")).to eq []
    end

    it "exists" do
      Tuna.create!(
        name: "Tuna name",
        origin: "Tuna origin",
        color: "white"
      )
      expect(Tuna.findByOrigin("Tuna origin")).to eq [Tuna.last]
    end
  end
end
