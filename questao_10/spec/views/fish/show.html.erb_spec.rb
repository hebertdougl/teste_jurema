require 'rails_helper'

RSpec.describe "fish/show", type: :view do
  before(:each) do
    @fish = assign(:fish, Fish.create!(
      :name => "Name",
      :origin => "Origin",
      :color => "Color"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Origin/)
    expect(rendered).to match(/Color/)
  end
end
