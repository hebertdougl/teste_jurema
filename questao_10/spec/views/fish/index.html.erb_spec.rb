require 'rails_helper'

RSpec.describe "fish/index", type: :view do
  before(:each) do
    assign(:fish, [
      Fish.create!(
        :name => "Name",
        :origin => "Origin",
        :color => "Color"
      ),
      Fish.create!(
        :name => "Name",
        :origin => "Origin",
        :color => "Color"
      )
    ])
  end

  it "renders a list of fish" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Origin".to_s, :count => 2
    assert_select "tr>td", :text => "Color".to_s, :count => 2
  end
end
