require 'rails_helper'

RSpec.describe "fish/new", type: :view do
  before(:each) do
    assign(:fish, Fish.new(
      :name => "MyString",
      :origin => "MyString",
      :color => "MyString"
    ))
  end

  it "renders new fish form" do
    render

    assert_select "form[action=?][method=?]", fish_index_path, "post" do

      assert_select "input#fish_name[name=?]", "fish[name]"

      assert_select "input#fish_origin[name=?]", "fish[origin]"

      assert_select "input#fish_color[name=?]", "fish[color]"
    end
  end
end
