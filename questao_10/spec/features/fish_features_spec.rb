describe "Fish", type: :feature do
  it "create fish" do
    visit '/fish/new'
    within("#new_fish") do
      fill_in 'Name', with: 'Fish Name'
      fill_in 'Origin', with: 'Fish Origin'
      fill_in 'Color', with: 'Fish Color'
    end
    click_button 'Create Fish'
    expect(page).to have_content 'Fish was successfully created'
  end

  it "not create invalid fish" do
    visit '/fish/new'
    within("#new_fish") do
      fill_in 'Origin', with: 'Fish Origin'
      fill_in 'Color', with: 'Fish Color'
    end
    click_button 'Create Fish'
    expect(page).to have_content 'Name can\'t be blank'
  end
end