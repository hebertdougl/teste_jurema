var ul = document.getElementById('list-number');
for (var i = 1; i <= 100; i++) {
  var li = document.createElement('li');
  ul.appendChild(li);
  if ( i % 3 == 0 && i % 5 == 0) {
    li.appendChild(document.createTextNode('FizzBuzz'));
  }else if(i % 3 == 0) {
    li.appendChild(document.createTextNode('Fizz'));
  }else if(i % 5 == 0){
    li.appendChild(document.createTextNode('Buzz'));
  }else{
    li.appendChild(document.createTextNode(i));
  }
};