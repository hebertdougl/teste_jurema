var ul = document.getElementById('list-number');
function loop (first, last) {
    if (first > last) {
        return;
    }
    var li = document.createElement('li');
    ul.appendChild(li);
    if ( first % 3 == 0 && first % 5 == 0) {
      li.appendChild(document.createTextNode('FizzBuzz'));
    }else if(first % 3 == 0) {
      li.appendChild(document.createTextNode('Fizz'));
    }else if(first % 5 == 0){
      li.appendChild(document.createTextNode('Buzz'));
    }else{
      li.appendChild(document.createTextNode(first));
    }

    return loop(first+1, last);
}
loop(1, 100);