# Respostas

## 1.

```js
$.ajax({
  type: "GET",
  url: "http://api.exemplo.com.br/estados",
  cache: false,
  success: function(response_data){
    response_data = response_data.estados
    for (x in response_data) {
      console.log("City: "+response_data[x].nome+" - UF:"+response_data[x].uf);
    }
  }
});
```

## 2.

index.html
```html
  <a href="#" id="mostrar-estados">Mostrar estados</a><br/>
  <div id="estado">
  </div>
```

application.js

```js
$(document).ready(function(){
  $("#mostrar-estados").click( function(){
    get_and_show_states();
  });
});


function get_and_show_states(){
  $.ajax({
    type: "GET",
    url: "http://api.exemplo.com.br/estados",
    cache: false,
    success: function(response_data){
      response_data = response_data.estados
      var div = document.getElementById('estado');
      var ul = document.createElement('ul');
      div.appendChild(ul);
      for (x in response_data) {
        var nome = response_data[x].nome;
        var uf = response_data[x].uf;
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(nome + " - " + uf))
        ul.appendChild(li);
      }
    }
  });
}
```

## 3.

Na tag display: none; não é alocado um espaço no entre as tags html em que o elemento está posicionado. Em visibility: hidden; a tag é renderizada na view mas não fica visível para o usuário, ela ocupa um espaço no html mas fica invisível.

## 4.

O id é um identificador dos elementos no qual deve ser único para cada elementos
  $('#id-example')
Já as classes são utilizadas para identificar um grupo de elementos que tem uma mesma característica de css, esta pode ser atribuída a vários elementos
  $('.class-example')

## 5.

Quando se trata de um sistema com html, js e css puros, separamos todos os arquivos por extensão e no html incluímos os arquivo js da seguinte maneira.
  <script src="example.js"></script>
Em projetos rails é utilizado como padrão um arquivo js para cada uma das modelos existentes e automaticamente ele é minificado e associado as respectivas views

## 6.

[Arquivo html da questão 6](questao_06.html)

## 7.

[Arquivo html da questão 7](questao_07.html)

## 8.

[Arquivo html da questão 8](questao_08.html)
[Arquivo js da questão 8](questao_08.js)

## 9.

```ruby
  thing ? "thing" : "nothing"
```
## 10.

Projeto desenvolvido em ruby on rails.

### Prerequisites

* Ruby 2.3+
* Rails 5.0+
* Postgresql 9.4+

### Tools

#### Run the test suite

```ruby
    $ rspec
```

## 11.

### A) Crie uma query que retorne todos os usuários criados no mês de janeiro de 2013, nos grupos (IDs) 15 e 40.

```sql
  SELECT * FROM users WHERE created > '2013-01-01 00:00:00';
```

### B) Crie uma query para autenticação do usuário (função hash SHA1).
  
```sql
  SELECT name, password FROM users where name = 'aaa' and password = sha1('21f892');
```

### C) Insira um usuário qualquer e defina que ele pertence aos grupos (IDs) 20 e 33.

```sql
  BEGIN TRANSACTION
    DECLARE @UserId int;
    INSERT INTO users (name, description) VALUES ("username", "user description")
    SELECT @UserId = scope_identity();
    INSERT INTO groups (id, users_id) VALUES (20, @UserId), (33, @UserId);
  COMMIT
```

### D) Crie uma query que retorne todos os usuários que possuem as permissões (IDs) 55, 80 e 48.

```sql
  SELECT * FROM users u 
  INNER JOIN groups g ON g.users_id = u.id 
  INNER JOIN permissions p ON p.groups_id = g.id
  WHERE p.id = 55 AND p.id = 80;
```

### E) Delete todos os dados de todas as tabelas.

```sql
TRUNCATE TABLE 'permissions', 'groups', 'users';
```

### 12.

```ruby
x, y = y, x
```

### 13.

Código no git

#### 14.
  1. Procure pelos processos zumbis:
    ps aux | grep 'Z'
  2. Visualize os PIDS dos processos zumbis (exceto a primeira linha)
  3. Mate os processos zumbis
    kill -9 last_pid

### 15.

```
  $ ps -aux | grep gnome
```

### 16.

  0 10 * * 5 /usr/local/foo-bar.sh
  22 22 * * 5 /usr/local/foo-bar.sh

### 17.

```ruby
x = x ^ y
y = x ^ y
x = x ^ y
```

### 18. 

[Arquivo html da questão 18](questao_18.html)
[Arquivo js da questão 18](questao_18.js)

### 19.

[Arquivo html da questão 19](questao_19.html)
[Arquivo js da questão 19](questao_19.js)
[Arquivo CSS da questão 19](questao_19.css)
